#!/bin/sh

set -eu

. ../../pgapt.conf

for FLAVOR in $REPO_DIST_FLAVORS; do
  case $FLAVOR in
    .) incoming="incoming"
       map_to_dist="testing"
       ;;
    testing)
      continue
      ;;
    snapshot)
      incoming="snapshot"
      map_to_dist="snapshot"
      ;;
  esac

cat <<EOF
Name: $incoming
IncomingDir: /srv/apt/$incoming
TempDir: /srv/apt/repo/tmp
LogDir: $incoming-log
Multiple: yes
# ignore .buildinfo files
Permit: unused_files
Cleanup: unused_files
Allow:
EOF
for DIST in $PG_REPOSITORY_DISTS ; do
  echo " $DIST$REPO_DIST_SUFFIX>$DIST$REPO_DIST_SUFFIX-$map_to_dist"
done
echo

done
