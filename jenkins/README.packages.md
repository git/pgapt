# Package notes

## Inter-package dependencies

* oracle-libs
  * oracle-fdw

* pgaudit-* (all of them)
  * pgauditlogtofile

* pglogical
  * pgl-ddl-deploy
  * pglogical-ticker
    * pg-fact-loader

* pgq
  * pgq-node
    * londiste-sql

* pgtap
  * mimeo
  * pg-partman

* postgis
  * pgrouting
  * h3-pg (also depends on libh3)
