#!/usr/bin/env bash

set -eu

# seed ssh-agent
cd "$(dirname $0)"
firsthost="$(head -n1 hosts | cut -d ' ' -f 1)"
set -x
ssh $firsthost true

ansible all "$@" -m shell -a 'sudo -i apt-get -qq update && sudo -i DEBIAN_FRONTEND=noninteractive apt-get -o DPkg::Options::=--force-confold -y --with-new-pkgs upgrade'
