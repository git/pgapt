BEGIN;

INSERT INTO architecture VALUES
    ('all'),
    ('amd64'),
    ('arm64'),
    ('ppc64el'),
    ('s390x');

COMMIT;
