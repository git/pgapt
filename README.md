# apt.postgresql.org

The main idea of this project is to provide PostgreSQL and its Extensions
packages for all supported PostgreSQL versions and debian version. That
means things like PostgreSQL 9.1 for `squeeze`, with all known debian
extensions, and for `sid` and `wheezy` too. And same for `8.3` and `8.4` and
`9.0` and `9.1` and `9.2`. Targeting `i386` and `amd64`.

https://wiki.postgresql.org/wiki/Apt

## Build System

The build system is currently based on jenkins and some scripting. `Jenkins`
is only about launching the build jobs, the real work is done in the scripts.

## Adding a new PostgreSQL version to the repository

* Update `PG_DEVEL_VERSION` and `PG_VERSIONS` in pgapt.conf
* Update jenkins/pgapt-jobs.yaml
* Run `make` and commit changes
* Update `pgdg/apt.postgresql.org.sh` in postgresql-common
* Update the repository master host: cd /srv/apt; sudo -u aptuser git pull
* Create new Jenkins jobs and build them

## Changing the default PostgreSQL version

* Update `PG_MAIN_VERSION` in pgapt.conf
  * Update the repository master host: cd /srv/apt; sudo -u aptuser git pull
  * Run apt.postgresql.org job
* In postgresql-common:
  * Run "make" to update `pgdg/apt.postgresql.org.sh`
  * Update debian/supported-versions
  * Install new postgresql-common version on pgdgbuild (generate-pgdg-source needs it)
* Rebuild the old default version so its libpq gets added to the extra component (as it was in main before)
* Rebuild the new default version so its libpq gets added to main (as it was in the extra component before)
* Possibly update the Jenkins pipeline jobs to stop triggering beta jobs
* To remove obsolete old lib packages, use
  ```
  . /srv/apt/pgapt.conf
  COMPONENT=11
  PKGS="libpq5 libecpg6 libecpg-compat3 libecpg-dev libpgtypes3 libpq-dev"
  for dist in $PG_SUPPORTED_DISTS; do for pkg in $PKGS; do sudo -u aptuser reprepro -C $COMPONENT remove $dist-pgdg-testing $pkg $pkg-dbgsym; done; done
  ```
* Removing obsolete PG component:
  ```
  . /srv/apt/pgapt.conf
  COMPONENT=13
  PKGS="libecpg6 libecpg-compat3 libecpg-dev libpgtypes3 libpq5 libpq-dev postgresql-$COMPONENT postgresql-$COMPONENT-dbg postgresql-client-$COMPONENT postgresql-contrib-$COMPONENT postgresql-doc-$COMPONENT postgresql-plperl-$COMPONENT postgresql-plpython3-$COMPONENT postgresql-pltcl-$COMPONENT postgresql-server-dev-$COMPONENT"
  for dist in $PG_SUPPORTED_DISTS; do for pkg in $PKGS; do sudo -u aptuser reprepro -C $COMPONENT remove $dist-pgdg-testing $pkg $pkg-dbgsym; done; done
  ```

## Changing the set of supported PostgreSQL versions

* In postgresql-common, update debian/supported-versions
* Trigger a postgresql-common build
* Upgrade postgresql-common on pgdgbuild.dus.dg-i.net because
  generate-pgdg-source uses the list when generating debian/control from
  debian/control.in
* Restrict deprecated version to `dist-filter: *lts_dist_filter`
* Announce change on wiki and the mailing list

## Adding a new distribution

Update these files in git:

* vi pgapt.conf jenkins/pgapt-jobs.yaml jenkins/generate-pgdg-source
* Check debhelper version in jenkins/generate-pgdg-source
* make && git commit -a

Update repository master host:

* cd /srv/apt; sudo -u aptuser git pull
* Seed initial packages:
  * sudo -u aptuser reprepro copysrc $newdist-pgdg-testing $olddist-pgdg-testing postgresql-common
    sudo -u aptuser reprepro copysrc $newdist-pgdg-testing $olddist-pgdg-testing pgdg-buildenv
    possibly: for version in $PG_VERSIONS; do sudo -u aptuser reprepro copysrc $newdist-pgdg-testing $olddist-pgdg-testing postgresql-$version; done
  * make sure to copy from a distribution that has all the target architectures (even if the package is arch:all, #926233)
* sudo -u aptuser reprepro export $newdist-pgdg (so apt can run update on it even if it's still empty)
* (possibly: sudo -u aptuser reprepro copymatched $newdist-pgdg $newdist-pgdg-testing \*)
* Wait until the mirror sync has run (xx:17)

Update Jenkins hosts:

* Run the "apt.postgresql.org" job
* Run the "sbuild-update" jobs

Build packages:

* Trigger rebuilds of the source packages:
  * for pkg in $(cat packages); do ./trigger-source-rebuild $pkg; done
* Trigger builds of the binary packages:
  * for pkg in $(cat packages); do ./trigger-job $pkg-binaries 'matrix=distribution%3d%3d"groovy"'; done

Do the paperwork:

* Update wiki pages
* Update pgdg/apt.postgresql.org.sh in postgresql-common
* Have pgweb updated
  * https://www.postgresql.org/download/linux/debian/
  * https://www.postgresql.org/download/linux/ubuntu/
* Send announcement

## Archiving a distribution to apt-archive.postgresql.org

* sudo -u aptuser repo/bin/copy-dist-to-archive $DIST-pgdg
* Announce and wait some time
* remove from pgapt.conf; make -C repo; git commit
* cd /srv/apt && sudo -u aptuser git pull
* sudo -u aptuser reprepro --delete clearvanished
* `sudo -u aptuser rm -rf repo/dists/$DIST-pgdg*`

## Adding a new architecture

Same as adding a new distribution, but instead of copysrc/export, use `flood`:
* for dist in ...; do sudo -u aptuser reprepro flood $dist-pgdg-testing $arch; done
* in the database on repo host: `insert into architecture values ('$arch');`
