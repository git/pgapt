#!/bin/sh

set -eu

. ../../pgapt.conf

for DIST in $PG_REPOSITORY_DISTS ; do
	set_dist_vars $DIST
	for FLAVOR in $REPO_DIST_FLAVORS ; do
		if [ "$FLAVOR" = "." ]; then
			D="$DIST$REPO_DIST_SUFFIX"
		else
			D="$DIST$REPO_DIST_SUFFIX-$FLAVOR"
		fi
		ARCHS=$(echo $ARCHS) # strip duplicate spaces
		COMPONENTS=$(echo $COMPONENTS) # strip duplicate spaces
		cat <<EOF
Codename: $D
Suite: $D
Origin: $REPO_ORIGIN
Label: $REPO_LABEL
Architectures: source $ARCHS
Components: $COMPONENTS
SignWith: $REPO_SIGN_WITH
Log: $D.log
Uploaders: uploaders
DebIndices: Packages Release . .gz .bz2
UDebIndices: Packages . .gz .bz2
DscIndices: Sources Release .gz .bz2
Tracking: minimal includebuildinfos keepsources
Contents: percomponent nocompatsymlink
EOF
		case $FLAVOR in *testing|*snapshot)
			echo "NotAutomatic: yes"
			echo "ButAutomaticUpgrades: yes"
			;;
		esac
		echo
	done
done
